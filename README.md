# CTF Bot

## Prerequisites Setup

Skip these steps if you are running an established CTF bot instance

### Discord Bot Creation

1. Create a new application on Discord
    * https://discord.com/developers/applications
2. Go to the "Bot" Settings and copy the token or generate a new one.
   This token will go in the `[discord]`.`token` field of the config or
   `CTF_BOT_DISCORD_TOKEN` in the `.env` file.

### OIDC Confidential Client

The bot leverages a keycloak confidential client as the authentication
mechanism to hasura. Setup a confidential client with the hasura mappers
allowing granting the role of `ctf_admin`

1. Create a new confidential client under "Clients". Copy the client id and
   client secret into the config file or `.env` file.
2. In Settings, enable the service account and press save. Under the new
   "Service Account Roles" tab, add the hasura client role required.
3. In Mappers, add the following:
    1. `x-hasura-default-role`
        1. Mapper Type: `Hardcoded claim`
        2. Token Claim Name: `https://hasura\.io/jwt/claims.`x-hasura-default-role
        3. Value: `ctf_admin`
        4. Claim JSON Type: `string`
    2. `x-hasura-allowed-roles`
        1. Mapper Type: `User Client Role`
        2. Client ID: `hasura`
        3. Multivalued: `ON`
        4. Token Claim Name: `https://hasura\.io/jwt/claims.`x-hasura-allowed-roles
        5. Claim JSON Type: `string`

## Configuration

### Runtime State

The bot uses a JSON file configured under `[runtime]`.`path` to track what has
been announced. If the bot crashes, this ensures that it will not spam the
channels or optionally if it misses and announcement, update the
`LatestChallengeTime` to a timestamp prior to the event missed and restart the
bot to ensure that event is announced.

### Get Discord Channel IDs

1. In Discord, right click the channel you want the bot to announce its
   presence in and copy the ID. Put this number in the
   `[discord]`.`staff_channels` field of the config or
   `CTF_BOT_DISCORD_STAFF_CHANNELS` in the `.env` file.
2. Do the same for the channel that you want the bot to announce updates to.
   Put this number in the `[discord]`.`announce_channels` field of the config
   or `CTF_BOT_DISCORD_ANNOUNCE_CHANNELS` of the `.env` file.
   * ![copy_ids](docs/images/copy_ctf_open_channel_id.png)

## Running with docker

One or more instances of the CTF Bot can be run using docker. To run more than
one instance, change `CTF_BOT_INSTANCES` environment variable to the desired
number of instances (e.g. `CTF_BOT_INSTANCES=2`). to Prior to running the bot
using docker, ensure all the fields are filled out in a `.env` file; a sample
of which is included in this repo. When running multiple instances, default
environment variables are read in first. Environment variables including a bot
instance number (e.g. `CTF_BOT_1_EVENT_ENVIRONMENT`) will overload the default
values.

See `sample.env` for a full configuration, however this is an example of
configuration for 2 instances running in docker.

```ini
# Environment variables with instance number will overwrite default variables
CTF_BOT_INSTANCES=2

# Environment variables for Bot instance 1
CTF_BOT_1_DATABASE_GQL_URI=https://vce1.hasura.app/v1/graphql
CTF_BOT_1_DISCORD_ANNOUNCE_CHANNELS=1075812692641726584
CTF_BOT_1_EVENT_ENVIRONMENT=vce1
CTF_BOT_1_RUNTIME_PATH=/app/cache/runtime1.json

# Environment variables for Bot instance 2
CTF_BOT_2_DATABASE_GQL_URI=https://vce2.hasura.app/v1/graphql
CTF_BOT_2_DISCORD_ANNOUNCE_CHANNELS=1075812820341489686
CTF_BOT_2_EVENT_ENVIRONMENT=vce2
CTF_BOT_2_RUNTIME_PATH=/app/cache/runtime2.json
```

After setup is complete, the bot can be run with

```bash
docker-compose up
```

**Note:** `network_mode: bridge` may be required on some hosts, particularly windows to connect.

## Building

When building from source, only one instance can be run per execution. There
are two ways to build the bot:

```bash
make
```

Or build using

```bash
go build
```

### Usage

Running the bot with no config option `-c` will generate a new config file.
When running the executable, you must have the environment variables set locally
on your machine or filled out in the `config.toml` file (a sample `config.toml`
is included in this repo). Environment variables will take precedence over the
config file.

```bash
./ctf_bot -c config.toml
```
