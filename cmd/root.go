package cmd

import (
	"ctf_bot/bot"
	"ctf_bot/config"
	"ctf_bot/database"
	"ctf_bot/discord"
	"ctf_bot/util"
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	loggerLevel zap.AtomicLevel
	loggerBasic *zap.Logger
	logger      *zap.SugaredLogger
	cfgFile     string
	botIndex    string
	ctfBot      *bot.Bot
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "ctf_bot",
	Short: "Integrate your competition with discord",
	Long: `CTF Bot is a discord bot to enhance the participant experience:

CTF Bot announces new CTF challenges, first blood CTF solves,
and provides custom commands to get status updates on the event.`,
	PreRun: Setup,
	Run: func(cmd *cobra.Command, args []string) {
		// This will run until exit where it will call os.Exit(rc)
		message := "CTF Bot for event " + ctfBot.Config.Event.Environment + " reporting for duty!"
		errs := discord.SendStaff(message)
		if errs != nil {
			util.LogErrors(logger, errs)
			logger.Fatal("could not announce bot ready on discord")
		}
		mainLoop(Reload, tick, Cleanup)
	},
}

// Cleanup performs any cleanup on required modules. Call before exit.
func Cleanup() {
	updateRuntime()
	ctfBot.Cleanup()
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

// SetLogger sets up the internal logger for the cmd package
func SetLogger(loggerIn *zap.Logger) {
	loggerBasic = loggerIn
	logger = loggerBasic.Sugar()
}

// SetLoggerLevel sets the desired log level
// Levels available: debug, info, warn, error, dpanic, panic, fatal
func SetLoggerLevel(level string) {
	switch level {
	case "debug":
		loggerLevel.SetLevel(zap.DebugLevel)
	case "info":
		loggerLevel.SetLevel(zap.InfoLevel)
	case "warn":
		loggerLevel.SetLevel(zap.WarnLevel)
	case "error":
		loggerLevel.SetLevel(zap.ErrorLevel)
	case "dpanic":
		loggerLevel.SetLevel(zap.DPanicLevel)
	case "panic":
		loggerLevel.SetLevel(zap.PanicLevel)
	case "fatal":
		loggerLevel.SetLevel(zap.FatalLevel)
	}
}

// Setup Sets up the command
func Setup(cmd *cobra.Command, args []string) {
	Reload()
}

// Reload Reloads the config
func Reload() {
	initConfig()
	cfg := new(config.BotConfig)
	if err := viper.Unmarshal(&cfg); err != nil {
		fmt.Println("Unmarshal of config failed:", err)
		os.Exit(1)
	}
	SetLoggerLevel(cfg.Log.Level)
	ctfBot.SetConfig(cfg)
	fmt.Printf("Default config: %s\n", defaultConfigFile())
	loadRuntime()
	ctfBot.Setup()
}

func defaultConfigPath() string {
	// Find home directory.
	home, err := os.UserHomeDir()
	if err != nil {
		panic("could not get user home dir")
	}
	p := path.Join(home, ".config", "ctf_bot")
	return p
}

func defaultConfigFile() string {
	p := path.Join(defaultConfigPath(), "config.toml")
	return p
}

func init() {
	ctfBot = new(bot.Bot)
	loggerLevel = zap.NewAtomicLevel()
	encoderCfg := zap.NewDevelopmentEncoderConfig()
	logger := zap.New(zapcore.NewCore(
		zapcore.NewJSONEncoder(encoderCfg),
		zapcore.Lock(os.Stdout),
		loggerLevel,
	))
	SetLogger(logger)
	bot.SetLogger(logger)
	database.SetLogger(logger)
	discord.SetLogger(logger)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	// Flag to take in the index of the bot, used for running multiple events, default 1
	rootCmd.PersistentFlags().StringVarP(&botIndex, "botIndex", "i", "1", "index of the bot being run")

	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", defaultConfigFile(), "config file (default is $HOME/.config/ctf_bot/config.toml)")
	rootCmd.PersistentFlags().StringP("token", "t", "YOUR_DISCORD_TOKEN", "Your discord token")
	err := viper.BindPFlag("discord.token", rootCmd.PersistentFlags().Lookup("token"))
	if err != nil {
		panic("could not bind token argument to discord.token configuration path")
	}
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {

		// Search config in home directory with name ".ctf_bot" (without extension).
		viper.AddConfigPath(defaultConfigPath())
		viper.SetConfigType("toml")
		viper.SetConfigName("config")
	}

	viper.SetEnvPrefix("CTF_BOT")

	viper.AutomaticEnv() // read in environment variables that match

	envMap := []string{
		"event.environment",
		"log.level",
		"database.gql_uri",
		"database.default_query_role",
		"database.tls_skip_verify",
		"database.unauthenticated_role",
		"database.request_timeout",
		"authentication.oidc_client_id",
		"authentication.oidc_client_secret",
		"discord.token",
		"discord.staff_channels",
		"discord.announce_channels",
		"runtime.announce_challenges",
		"runtime.announce_first_solve",
		"runtime.check_interval",
		"runtime.path",
	}

	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	for _, v := range envMap {
		err := viper.BindEnv(v)
		if err != nil {
			fmt.Fprintln(
				os.Stderr,
				"could not setup environment variable mapping:")
			return
		}
	}

	// Change EvnPrefix to include the botIndex from the argument
	viper.SetEnvPrefix("CTF_BOT_" + botIndex)

	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	for _, v := range envMap {
		err := viper.BindEnv(v)
		if err != nil {
			fmt.Fprintln(
				os.Stderr,
				"could not setup environment variable mapping:")
			return
		}
	}

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}

func tick() int {
	ctfBot.Tick()
	updateRuntime()
	return 0
}
