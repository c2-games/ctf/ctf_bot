package cmd

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"path"
	"time"
)

type runtimeState struct {
	LatestChallengeTime string
	AnnouncedFirstSolve []string
}

func loadRuntime() {
	var err error
	var runtime runtimeState
	file := ctfBot.Config.Runtime.Path

	// Initialize to current time so we only announce newly released challenges
	now := time.Now().UTC()
	ctfBot.LatestChallengeTime = &now

	_, err = os.Stat(file)
	if err != nil {
		// it is fine if there is no file
		return
	}

	fd, err := os.Open(file)
	if err != nil {
		log.Fatal("Runtime config file could not be opened: ", file)
	}

	jsonBytes, err := ioutil.ReadAll(fd)
	if err != nil {
		log.Fatal("Runtime config file could not be read: ", file)
	}

	if err := json.Unmarshal(jsonBytes, &runtime); err != nil {
		log.Fatal(err)
	}

	parsed, err := time.Parse(time.RFC3339Nano, runtime.LatestChallengeTime)
	if err == nil {
		ctfBot.LatestChallengeTime = &parsed
	} // If there was an error, this is just set to now above
	ctfBot.AnnouncedFirstSolve = runtime.AnnouncedFirstSolve
}

func updateRuntime() {
	var runtime runtimeState
	if ctfBot.LatestChallengeTime != nil {
		runtime.LatestChallengeTime = ctfBot.LatestChallengeTime.Format(time.RFC3339Nano)
	} else {
		runtime.LatestChallengeTime = time.Now().UTC().Format(time.RFC3339Nano)
	}
	runtime.AnnouncedFirstSolve = ctfBot.AnnouncedFirstSolve

	jsonData, err := json.MarshalIndent(runtime, "", "    ")
	if err != nil {
		logger.Fatal(err)
	}

	// Ensure directory exists
	dirpath := path.Dir(ctfBot.Config.Runtime.Path)
	if err := os.MkdirAll(dirpath, os.ModePerm); err != nil {
		log.Fatal(err)
	}

	f, err := os.OpenFile(ctfBot.Config.Runtime.Path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)

	if err != nil {
		logger.Debug(string(jsonData))
		logger.Fatal(err)
	}

	_, err = f.WriteString(string(jsonData))
	if err != nil {
		logger.Debug(string(jsonData))
		logger.Fatal(err)
	}

	err = f.Close()
	if err != nil {
		logger.Debug(string(jsonData))
		logger.Fatal(err)
	}
}
