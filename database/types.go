package database

import "time"

// Challenge - a relationship from the active table to a challenge
type Challenge struct {
	ID       string `json:"id,omitempty"`
	Name     string `json:"name,omitempty"`
	Category string `json:"category,omitempty"`
}

// Active - a relationship from the active table
type Active struct {
	EventID     string    `json:"event_id"`
	Challenge   Challenge `json:"challenge"`
	Points      float32   `json:"points,omitempty"`
	ReleaseTime time.Time `json:"release_time,omitempty"`
}

// ChallengeSolves - a relationship from the solves table
type ChallengeSolves struct {
	EventID     string    `json:"event_id"`
	Active      Active    `json:"challenge"`
	Team        Team      `json:"team"`
	Points      float32   `json:"points,omitempty"`
	ReleaseTime time.Time `json:"release_time,omitempty"`
	Time        time.Time `json:"time,omitempty"`
}

// Team - a relationship from the team table
type Team struct {
	ID   string `json:"id,omitempty"`
	Name string `json:"name"`
}
