package discord

import (
	"ctf_bot/config"
	"fmt"

	"github.com/bwmarrin/discordgo"
	"go.uber.org/zap"
)

var (
	loggerBasic    *zap.Logger
	logger         *zap.SugaredLogger
	cfg            *config.DiscordConfig
	discordSession *discordgo.Session
)

// SetLogger sets the logger
func SetLogger(loggerIn *zap.Logger) {
	loggerBasic = loggerIn
	logger = loggerBasic.Sugar()
}

// Setup sets up the discord session
func Setup(cfgIn *config.DiscordConfig) error {
	var err error
	cfg = cfgIn
	// Create a new Discord session using the provided bot token.
	discordSession, err = discordgo.New("Bot " + *cfg.Token)
	if err != nil {
		logger.Error("error creating discord session,")
		return err
	}

	botUser, err := discordSession.User("@me")
	if err != nil {
		logger.Error("error getting bot user details")
		return err
	}
	logger.Infof(
		"Logged in as: ID: %s, Email: %s, Username: %s",
		botUser.ID, botUser.Email, botUser.Username,
	)

	err = discordSession.Open()
	if err != nil {
		logger.Error("error opening discord session")
		return err
	}

	return nil
}

// Cleanup cleans up the discord session
func Cleanup() {
	if discordSession != nil {
		discordSession.Close()
	}
	discordSession = nil
}

// SendChannels sends a message to the channels provided
func SendChannels(msg string, channels []string) []error {
	var errors []error
	for _, channel := range channels {
		_, err := discordSession.ChannelMessageSend(channel, msg)
		if err != nil {
			e := fmt.Errorf(
				"error announcing on channel \"%s\": \"%s\": \"%s\"",
				channel, msg, err.Error(),
			)
			errors = append(errors, e)
		}
	}
	if len(errors) > 0 {
		return errors
	}
	return nil
}

// SendStaff sends a message to the staff channel
func SendStaff(msg string) []error {
	return SendChannels(msg, cfg.StaffChannels)
}

// SendAnnounce sends a message to the public channel
func SendAnnounce(msgs []string) []error {
	var errors []error
	const MsgMax = 2000 // Errors sending larger than this from discord
	var messages []string
	var msg string
	for _, m := range msgs {
		// If any single message is larger than MsgMax
		// don't send any of them.
		if len(m) > MsgMax {
			errors = append(errors, fmt.Errorf("message too large"))
			return errors
		}
		// If appending this message would breach MsgMax
		// then queue the message for sending
		if len(msg)+len(m) > MsgMax {
			messages = append(messages, msg)
			msg = ""
		}
		// If message has content, append with a newline
		if len(msg) > 0 {
			msg += "\n" + m
		} else {
			// Otherwise just use the content of m
			msg = m
		}
	}
	// Add any remaining messages that need to be announced
	if len(msg) > 0 {
		messages = append(messages, msg)
	}
	for _, m := range messages {
		errs := SendChannels(m, cfg.AnnounceChannels)
		if errs != nil {
			errors = append(errors, errs...)
		}
	}
	if len(errors) > 0 {
		return errors
	}
	return nil
}
