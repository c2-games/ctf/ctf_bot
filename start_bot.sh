#!/bin/sh

number_of_events=$1

for i in $(seq 1 "$number_of_events")
do
    ./ctf_bot -i "$i" -c config.toml > /app/cache/"bot$i.log" 2>&1 &
done

sleep 5

tail -f -n +1 /app/cache/*.log
