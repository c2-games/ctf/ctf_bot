package util

import "go.uber.org/zap"

// LogErrors logs an array of errors
func LogErrors(logger *zap.SugaredLogger, errs []error) {
	for _, err := range errs {
		logger.Error(err.Error())
	}
}

// StrListContains - search the given list []string for the given string
func StrListContains(list []string, value string) bool {
	for _, v := range list {
		if v == value {
			return true
		}
	}
	return false
}

// StrListDelete - delete the given string from the list []string
func StrListDelete(list []string, value string) []string {
	for i, v := range list {
		if v == value {
			return append(list[:i], list[i+1:]...)
		}
	}
	return list
}
